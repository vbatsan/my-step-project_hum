//Mansonry plagin
window.onload = function() {
    $('.gallary-imeges').masonry({
        itemSelector: '.gallary-img',
        gutter: 15,
    });
};


// swhitch tabs
$('.tabs-link').click(switchTab);

function switchTab() {
    $('.tabs-link').removeClass("tabs-active");
    $('.tabs-content').removeClass('show-tab');
    let elem = $(this).data('target');
    console.log(elem);
    $(`#${elem}`).addClass('show-tab');
    $(this).addClass('tabs-active');
}
//filter img
$('.work-pic').hide();
$('.work-pic:hidden').slice(0,12).show();

$('.work-link').click(imgFilter);

function imgFilter() {
    $('.work-btn .load-btn').show();
    $('.work-pic').hide();
    $('.work-link').removeClass('work-link-active');
    let showElem = $(this).data("target");
    $(`.work-pic.${showElem}:hidden`).slice(0,12).show();
    $(this).addClass('work-link-active');
    if (!$(`.work-pic.${showElem}:hidden`).length){ $('.work-btn .load-btn').hide() }
}

//work load btn

$('.loading').hide();
$('.work-btn .load-btn').click(showMore);

function showMore() {
    $(this).hide();
    $('.loading').show();
    setTimeout(function () {
        $('.loading').hide();
        let picsType = $('.work-link.work-link-active').data('target');
        let pics = $(`.work-pic.${picsType}:hidden`);
        pics.slice(0,12).show();
        let elems = pics.length;
        elems = elems - 12;
        if (elems > 0) $('.work-btn .load-btn:hidden').show();
    },2000);

}

//gallary load btn
$('.gallary-img').hide();
$('.gallary-img:hidden').slice(0,12).show();
$('.gallary-btn .load-btn').click(showMoreCuted);

function showMoreCuted() {
    $(this).hide();
    $('.loading').show();
    setTimeout(function () {
        $('.loading').hide();
        let pics = $('.gallary-img:hidden');
        pics.slice(0,12).show();
        let elems = pics.length;
        elems = elems - 12;
        if (elems > 0) $('.gallary-btn .load-btn:hidden').show();
    },2000)
}

//slider

$('.fback').hide();
$('.fback.first:hidden').show();
$('.introduse').hide();
$('.introduse.first:hidden').show();
$('.logo').hide();
$('.logo.first:hidden').show();

$('.mini-logs').click(switchPers);

function switchPers() {
    $('.mini-logs').removeClass('mini-logs-active');
    $(this).addClass('mini-logs-active');
    let type = $(this).data('type');
    $('.fback').hide();
    $(`.fback.${type}:hidden`).show();
    $('.introduse').hide();
    $(`.introduse.${type}:hidden`).show()
    $('.logo').hide();
    $(`.logo.${type}:hidden`).show()
}

$('.right-arroy').click(function () {
    if ($('.mini-logs.mini-logs-active').next().hasClass("mini-logs")) {
        let type = $('.mini-logs.mini-logs-active').next('.mini-logs').data('type');
        $('.mini-logs').removeClass('mini-logs-active');
        $('.fback').hide();
        $(`.fback.${type}:hidden`).show();
        $('.introduse').hide();
        $(`.introduse.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
        let elem = $(`[data-type = ${type}`);
        $(elem).addClass('mini-logs-active');
    }
    else {
        $('.mini-logs').removeClass('mini-logs-active');
        $('.mini-logs').first().addClass("mini-logs-active");
        let type = $('.mini-logs.mini-logs-active').data('type');
        $('.fback').hide();
        $(`.fback.${type}:hidden`).show();
        $('.introduse').hide();
        $(`.introduse.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
    }
});

$('.left-arroy').click(function () {
    if ($('.mini-logs.mini-logs-active').prev().hasClass("mini-logs")) {
        let type = $('.mini-logs.mini-logs-active').prev('.mini-logs').data('type');
        $('.mini-logs').removeClass('mini-logs-active');
        $('.fback').hide();
        $(`.fback.${type}:hidden`).show();
        $('.introduse').hide();
        $(`.introduse.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
        let elem = $(`[data-type = ${type}`);
        $(elem).addClass('mini-logs-active');
    }
    else {
        $('.mini-logs').removeClass('mini-logs-active');
        $('.mini-logs').last().addClass("mini-logs-active");
        let type = $('.mini-logs.mini-logs-active').data('type');
        $('.fback').hide();
        $(`.fback.${type}:hidden`).show();
        $('.introduse').hide();
        $(`.introduse.${type}:hidden`).show()
        $('.logo').hide();
        $(`.logo.${type}:hidden`).show()
    }
});

function addShowBlock() {

}